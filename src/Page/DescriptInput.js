import React, {useState} from "react";
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
    root: {
width: '300px'
        },
    }));


export default function DescriptInput() {
    const classes = useStyles();
    let [inputness,setInputness] = useState('')

    return (
        <form className={classes.root} noValidate autoComplete="off" >
            <TextField id="ratingDescription" variant="outlined" size='small' fullWidth value={inputness} onChange={setInputness}/>
        </form>
    );
}
