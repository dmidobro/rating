import React, {useState} from "react";
import Rating from "@material-ui/lab/Rating";
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import DescriptInput from "./DescriptInput";
import TextField from "@material-ui/core/TextField";


const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexDirection: 'column-reverse',
    }
}));

export default function CheckboxList(props) {
    const classes = useStyles();
    let number = props.value

    return (
        <List className={classes.root}>
            {[...Array(props.value)].map((value, index) => {
                return (
                    <ListItem key={index}>
                        <ListItemIcon>
                            <Box component="fieldset" mb={3} borderColor="transparent" className={classes.box}>
                                <Rating max={number} size={"large"}
                                        name="simple-controlled"
                                        value={index + 1}
                                        readOnly
                                />
                            </Box>
                            <TextField variant="outlined" size='small' fullWidth value={props.starsDescription[index] || ''}
                                       onChange={(event) => props.onDescriptionChange(index, event.target.value)}/>
                        </ListItemIcon>
                    </ListItem>
                );
            })}
        </List>
    );
}