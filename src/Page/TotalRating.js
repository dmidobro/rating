import React from 'react';
import Rating from '@material-ui/lab/Rating';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import StarBorderIcon from '@material-ui/icons/StarBorder';

export default function TotalRating(props) {
    let value = props.value
    return (
        <div>
            <Box component="fieldset" mb={3} borderColor="transparent" >
                <Typography component="legend" align='center'>Total amount</Typography>
                <Rating max={10} size={"large"}
                        name="simple-controlled"
                        value={value}
                        readOnly
                        emptyIcon={<StarBorderIcon fontSize="inherit" />}
                />
            </Box>
        </div>
    );
}
