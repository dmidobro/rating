import React, {useState} from "react";
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import TotalRating from "./TotalRating";
import RatingDescription from "./RatingDescription";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles((theme) => ({

    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        width: '100%'
    },
    display: {
        display: "flex",
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection:'column',
        width: '100%'

    },
    description: {
        display: "flex",
        alignItems: 'center',
        width: "200px"
    },

}));

export default function SelectInput(props) {
    const classes = useStyles();
    const [age, setAge] = React.useState(1);
    const [open, setOpen] = React.useState(false);

    const [starsDescription,setStarsDescription] = useState([]);

    const handleChange = (event) => {
        console.log(event);
        setAge(event.target.value);
    setStarsDescription(starsDescription.slice(0, event.target.value));
    };
    const handleClose = () => {
        setOpen(false);
    };

    const handleOpen = () => {
        setOpen(true);
    };

    const changeStartDescription = (index, description) =>{
        const tmpStarsDescription = [...starsDescription];
        tmpStarsDescription[index] = description;
        setStarsDescription(tmpStarsDescription);
    }

    return (
        <div className={classes.display}>
            <div>Please Select Rating:</div>
            <FormControl className={classes.formControl}>
                <Select fullWidth={false}
                        labelId="change-rating"
                        id="controlled-open-select"
                        open={open}
                        onClose={handleClose}
                        onOpen={handleOpen}
                        value={age}
                        onChange={handleChange}
                > <MenuItem value={1}>1</MenuItem>
                    <MenuItem value={2}>2</MenuItem>
                    <MenuItem value={3}>3</MenuItem>
                    <MenuItem value={4}>4</MenuItem>
                    <MenuItem value={5}>5</MenuItem>
                    <MenuItem value={6}>6</MenuItem>
                    <MenuItem value={7}>7</MenuItem>
                    <MenuItem value={8}>8</MenuItem>
                    <MenuItem value={9}>9</MenuItem>
                    <MenuItem value={10}>10</MenuItem>
                </Select>
                <Button onClick={()=>console.log(starsDescription)} type="submit" variant="contained" color="primary">
                    Save
                </Button>
            </FormControl>
            {/*<TotalRating value={age}/>*/}
            <RatingDescription onDescriptionChange={changeStartDescription} starsDescription={starsDescription}
                               value={age} style={classes.description}/>
        </div>

    );
}


